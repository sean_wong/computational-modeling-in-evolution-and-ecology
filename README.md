# BIEB 143 Classwork

This repository contains work from the [Computational Modeling in Evolution and Ecology class at UCSD](https://gitlab.com/sean_wong/computational-modeling-in-evolution-and-ecology/blob/master/BIEB143Spr2019Syllabus.pdf)

## Contents
Assignment 1: [Introduction to R Part 1](https://gitlab.com/sean_wong/computational-modeling-in-evolution-and-ecology/blob/master/public/Week1WednesdayHomework.R)

Assignment 2: [Introduction to R Part 2](https://gitlab.com/sean_wong/computational-modeling-in-evolution-and-ecology/blob/master/public/Week1FridayHomework.R)

Assignment 3: [Introduction to R Part 3](https://gitlab.com/sean_wong/computational-modeling-in-evolution-and-ecology/blob/master/public/Week2WednesdayHomework.R)

Assignment 4: [Simulating a Poisson Distribution](https://gitlab.com/sean_wong/computational-modeling-in-evolution-and-ecology/blob/master/public/HW4Submission.R)

Assignment 5: [Population Genetics](https://gitlab.com/sean_wong/computational-modeling-in-evolution-and-ecology/blob/master/public/SeanWongPopGen.R)

Assignment 6: [Modeling Sexual Selection via Neural Networks](https://gitlab.com/sean_wong/computational-modeling-in-evolution-and-ecology/blob/master/public/SeanWong_NeuralNetwork.R)

Assignment 7: [Epidemiology](https://gitlab.com/sean_wong/computational-modeling-in-evolution-and-ecology/blob/master/public/SeanWongEpidemiology.R)

Assignment 8: [Game Theory](https://gitlab.com/sean_wong/computational-modeling-in-evolution-and-ecology/blob/master/public/SeanWongGameTheoryP1.R)

Assignment 9: [Game Theory (Further Mutations)](https://gitlab.com/sean_wong/computational-modeling-in-evolution-and-ecology/blob/master/public/SeanWongGameTheoryP2.R)
