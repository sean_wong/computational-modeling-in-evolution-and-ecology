########## Activities 1-3 ############# Sean Wong

# Write a script (NOT A FUNCTION!) that simulates throwing balls into urns (Activity 1),
# creates a histogram of the number of balls in each urn (Activity 2),
# and prints the index of dispersion to the console (Activity 3).

# If you don't like balls and urns, you can call your variables nBees and nFlowers, 
# nVirions and nCells, or whatever you want.

print("---------------------------------------------")
print("Activities 1-3")

#1
#No because the balls in urns should follow a normal distribution in terms of number of balls in each urn
#On average, each urn will hold nBalls/nUrns number of balls
#No, that is not expected
#No, that is not expected

nUrns = numeric(150)
nBalls = 20000
if (nBalls != 0)
{
  for (i in 1:nBalls)
  {
    rand = runif(1,1,length(nUrns)+1)
    nUrns[rand] = nUrns[rand] + 1
    nBalls = nBalls - 1
  }
}
#print(nUrns)

#2
hist(nUrns, 50)
#The structure of the histogram is still the same (normal distribution) despite changing the number of balls and urns.

#3
indexOfDisp = var(nUrns)/mean(nUrns)
print(indexOfDisp)

######### Activities 4-5 ##############

# Now package your balls and urns script from above into a FUNCTION (Activity 4).
# Call the function throwBallsIntoUrns.
# The function should take two inputs: nBalls and nUrns. (Or nBees and nFlowers, etc.)
# Then write a SCRIPT (not a function!) to run throwBallsIntoUrns() 1000 times,
# store the indices of dispersion, and graph them in a histogram.

print("---------------------------------------------")
print("Activities 4-5")

#4
throwBallsIntoUrns = function(numBalls,numUrns)
{
  nUrns = numeric(numUrns)
  nBalls = numBalls
  if (nBalls != 0)
  {
    for (i in 1:nBalls)
    {
      rand = runif(1,1,numUrns+1)
      nUrns[rand] = nUrns[rand] + 1
      #nBalls = nBalls - 1
    }
  }
  return(var(nUrns)/mean(nUrns))
}
#No, it is unlikely that one will get the same index of dispersion.

#5
holes = c()
for (i in 1:1000)
{
  holes = c(holes, throwBallsIntoUrns(1321,22))
}
hist(holes)

# Question 5 asks you to think about the relationship between mean and variance.
# Type your answers in the blank print statement below
print("---------------------------------------------")
print('Answer to thought questions in #5')
print('This histogram looks like a normal curve in which the most values fall under the mean and other values become less frequent the further away from the mean. 
      The mean value of the indexes of dispersion is .9971079 for this iteration. 
      Yes because the mean value of the indexes of dispersion is close to 1, this means that the variance of the indexes of dispersion and the mean of the indexes of dispersion are about equal.')

######### Activities 6-7 ###############
print("Activities 6-7")

#6
#Having attraction will make the distribution skewed towards the right because of higher indexes of distribution.
attraction = function(ballsinUrns)
{
  maxBalls = max(ballsinUrns)
  numMax = 0
  for (j in ballsinUrns)
  {
    if (j == maxBalls)
    {
      numMax = numMax + 1
    }
  }
  totalBalls = sum(ballsinUrns)
  totalUrns = length(ballsinUrns)
  chance = numeric(totalUrns)
  chance[1:totalUrns] = 1 - (totalUrns-1)*(1/totalUrns)
  for (i in 1:totalUrns)
  {
    if (totalBalls == 0)
    {
      "pass"
    }
    else if (totalBalls-(numMax * maxBalls) == 0)
    {
      "pass"
    }
    else if (ballsinUrns[i] == maxBalls)
    {
      chance[i] = chance[i] + ((maxBalls/totalBalls)/numMax)
    }
    else if (ballsinUrns[i] < maxBalls)
    {
      chance[i] = chance[i] - (maxBalls/totalBalls)*(ballsinUrns[i]/(totalBalls-(numMax*maxBalls)))
      if (chance[i] < 0)
      {
        chance[i] = 0
      }
    }
  }
  return(chance)
}

#Having repulsion will make the distribution skewed towards the left because of lower indexes of distribution.
repulsion = function(ballsinUrns)
{
  maxBalls = max(ballsinUrns)
  numMax = 0
  for (j in ballsinUrns)
  {
    if (j == maxBalls)
    {
      numMax = numMax + 1
    }
  }
  totalBalls = sum(ballsinUrns)
  totalUrns = length(ballsinUrns)
  chance = numeric(totalUrns)
  chance[1:totalUrns] = 1 - (totalUrns-1)*(1/totalUrns)
  for (i in 1:totalUrns)
  {
    if (totalBalls == 0)
    {
      "pass"
    }
    else if (totalBalls-(numMax * maxBalls) == 0)
    {
      "pass"
    }
    else if (ballsinUrns[i] == maxBalls)
    {
      chance[i] = chance[i] - ((maxBalls/totalBalls)/numMax)
      if (chance[i] < 0)
      {
        chance[i] = 0
      }
    }
    else if (ballsinUrns[i] < maxBalls)
    {
      chance[i] = chance[i] + (maxBalls/totalBalls)*(ballsinUrns[i]/(totalBalls-(numMax*maxBalls)))
    }
  }
  return(chance)
}

ballsAttracting = function(numBalls,numUrns)
{
  nUrns = numeric(numUrns)
  nBalls = numBalls
  if (nBalls != 0)
  {
    for (i in 1:nBalls)
    {
      chance = attraction(nUrns)
      rand = sample(1:length(nUrns),1,prob=chance)
      nUrns[rand] = nUrns[rand] + 1
      nBalls = nBalls - 1
    }
  }
  return(var(nUrns)/mean(nUrns))
}

ballsRepelling = function(numBalls,numUrns)
{
  nUrns = numeric(numUrns)
  nBalls = numBalls
  if (nBalls != 0)
  {
    for (i in 1:nBalls)
    {
      chance = repulsion(nUrns)
      rand = sample(1:length(nUrns),1,prob=chance)
      nUrns[rand] = nUrns[rand] + 1
      nBalls = nBalls - 1
    }
  }
  return(var(nUrns)/mean(nUrns))
}

holes2 = c()
for (i in 1:1000)
{
  holes2 = c(holes2, ballsAttracting(1000,100))
}
hist(holes2)

holes3 = c()
for (i in 1:1000)
{
  holes3 = c(holes3, ballsRepelling(1000,100))
}
hist(holes3)

#7
#Different sized urns result in the same normal distribution, but the balls tend to end up in the larger urns. 
sizeBalls = function(nBalls, lnUrns, mnUrns, snUrns)
{
  large = numeric(lnUrns)
  medium = numeric(mnUrns)
  small = numeric(snUrns)
  total = sum(length(large)+length(medium)+length(small))
  chance = c(.5,.35,.15)
  for (i in 1:total)
  if (nBalls != 0)
  {
    for (i in 1:nBalls)
    {
      rand = sample(1:3,1,prob=chance)
      rand1 = runif(1,1,length(large)+1)
      rand2 = runif(1,1,length(medium)+1)
      rand3 = runif(1,1,length(small)+1)
      if (rand == 1)
      {
        large[rand1] = large[rand1] + 1
        nBalls = nBalls - 1
        
      }
      else if (rand == 2)
      {
        medium[rand2] = medium[rand2] + 1
        nBalls = nBalls - 1
      }
      else if (rand == 3)
      {
        small[rand3] = small[rand3] + 1
        nBalls = nBalls - 1
      }
    }
  }
  vUrns = sum(var(large),var(medium),var(small))
  mUrns = sum(mean(large),mean(medium),mean(small))
  return(vUrns/mUrns)
}

holes4 = c()
for (i in 1:1000)
{
  holes4 = c(holes4, sizeBalls(5000,10,10,10))
}
hist(holes4)

"############## CoOkIe DaTa SiMuLaTiOn #############"

# Make sure to include any functions (throwBallsIntoUrns, etc) that you will use.
# The line below reads in the data and stores it in the vector variable cookieData.
# Once you run this line, the variable cookieData will be defined and should show
# up in your environment pane.
source("http://labs.biology.ucsd.edu/rifkin/courses/bieb143/spr18/cookieData.R")
cookies = length(cookieData2018)
chips = mean(cookieData2018)*cookies

# Declare your functions here.
indexofD = function(nUrns)
{
  return(var(nUrns)/mean(nUrns))
}

actualIoD = indexofD(cookieData2018)
print(actualIoD)

bagOfCookies = function(numCookies=cookies,numChips=chips)
{
  nUrns = numeric(numCookies)
  nBalls = numChips
  if (nBalls != 0)
  {
    for (i in 1:nBalls)
    {
      rand = runif(1,1,numCookies+1)
      nUrns[rand] = nUrns[rand] + 1
    }
  }
  return(var(nUrns)/mean(nUrns))
}

# Here is a test dataset to make sure your indexOfDispersion function works:
dataForTestOfIoD=c(5,1,5,6,7,3,45,7,3,4,7,8,5,5,7,35,7,3,2,34,6,7,7,34,4,4,6)
#print(indexofD(dataForTestOfIoD))
# The index of dispersion should be:  14.06655

# Now write code that will simulate the cookie data randomly,
# generate many indices of dispersion,
box = c()
for (i in 1:1000)
{
  box = c(box,bagOfCookies())
}
hist(box)
# and compare the index of dispersion of cookieData
# to the distribution of those simulated indices.
comparison = sum(box < actualIoD)
# Your code should print a p-value to the console.
pval = comparison/1000
print(pval)
#Based on the simulation, it is not likely that the real data can be acquired by chance. This suggests that Chip's Ahoy has some method to reduce the randomness of the chips in the cookies, such as through quality control. In addition, the p-value is not o, but rather, extremely small.